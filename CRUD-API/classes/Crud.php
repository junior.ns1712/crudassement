<?php

class CRUD{

    //define properties
    public $id;
    public $name;
    public $email;
    public $mobile;
    public $address;
    

    private $connection;
    private $addressbook_tbl;

    public function __construct($db){
        $this->connection = $db;
        $this->addressbook_tbl = "addressbook";
    }

    //create record
    public function create(){
        $user_query = "INSERT INTO ".$this->addressbook_tbl." SET name = ?, email = ?, address = ?, mobile = ? ";

        $user_obj = $this->connection->prepare($user_query);

        $user_obj->bind_param("ssss", $this->name, $this->email, $this->address, $this->mobile);

        if($user_obj->execute()){
            return true;
        }
        return false;
    }

     //read records
     public function read(){

        $list_query = "SELECT * FROM ".$this->addressbook_tbl." ORDER BY id ASC";

        $list_obj = $this->connection->prepare($list_query);

        $list_obj->execute();

        return $list_obj->get_result();
    }
    
     //Update record
    public function update(){
        $user_query = "UPDATE ".$this->addressbook_tbl." SET name = ?, email = ?, address = ?, mobile = ?  WHERE id = ?";

        $user_obj = $this->connection->prepare($user_query);

        $user_obj->bind_param("ssssi", $this->name, $this->email, $this->address, $this->mobile, $this->id);

        if($user_obj->execute()){
            return true;
        }
        return false;
    }

     //delete record
    public function delete(){

        $delete_obj = $this->connection->prepare("DELETE FROM ".$this->addressbook_tbl." WHERE id = ?");
        
        $delete_obj->bind_param("i", $this->id);

        $delete_obj->execute();
        
        if($delete_obj->execute()){
            return true;
        }
        return false;
    }

    //check if email exists already
    public function check_email(){
        $email_query = "SELECT * FROM ".$this->addressbook_tbl." WHERE email = ?";

        $usr_obj = $this->connection->prepare($email_query);

        $usr_obj->bind_param("s", $this->email);

        if($usr_obj->execute()){
            $data = $usr_obj->get_result();
            return $data->fetch_assoc();
        }

        return array();
    }

}