<?php

class Database{
    //variable declaration
    private $hostname;
    private $dbname;
    private $usernames;
    private $password;
    private $connection;

    public function connect(){
        //variable initialization

        $this->hostname = "localhost";
        $this->dbname = "kat_CRUD";
        $this->username = "kat_root";
        $this->password = "localhost";

        $this->connection = new mysqli($this->hostname, $this->username, $this->password, $this->dbname);

        if ($this->connection->connect_errno){
            //true => it means that it has some error,
            print_r($this->connection->connect_error);
            exit;
        }
        else{
            //false => it means no error in connection details
            return $this->connection;
            // echo "Successfully connected to db";
            // print_r($this-connection);
        }
    }
}

?>